$(document).ready(function () {
      // for every slide in carousel, copy the next slide's item in the slide.
      // Do the same for the next, next item.
      $('.multi-item-carousel .item').each(function () {
      var next = $(this).next();
      if (!next.length) {
      next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
      if (next.next().length > 0) {
      next.next().children(':first-child').clone().appendTo($(this));
      } else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
      }
      });
      jQuery('#btnAddAmmendmentsRow').click(function (event) {
      event.preventDefault();
      var newRow = jQuery('<tr><td><input id="txtExtraAmmendmentName" type="text" class="grid-textbox" /></td><td><input id="txtExtraAmmendmentNarration" type="text" class="grid-textbox" /></td></tr>');
      jQuery('#tblCustoAmmendmends').append(newRow);
      });
      jQuery('#btnAddFilesRow').click(function (event) {
      event.preventDefault();
      var newRow = jQuery('<tr><td><input id="txtCustomFileName" type="text" class="grid-textbox" /></td><td><input id="txtCustomFileExpiryDate" type="date" class="grid-textbox" /></td><td><input id="flCustomeFile" type="file" multiple accept="application/msword,text/plain, application/pdf/*" class="" /></td></tr>');
      jQuery('#tblCustomFiles').append(newRow);
      });
      $("#btnEditGeneralDetails").click(function (e) {
      $('#GeneralDetailsPop').simplePopup();
      });
      $("#btnUpdateGeneralDetails").click(function (e) {
      $(".simplePopupClose").click();
      $('#Alertpopup').modal('show');
      });
      $("#btnEditRackingDetails").click(function (e) {
      $('#RackingDetailsPop').simplePopup();
      });
      $("#btnUpdateRackingDetails").click(function (e) {
      $(".simplePopupClose").click();
      $('#Alertpopup').modal('show');
      });
      $("#btnEditAmenitiesDetails").click(function (e) {
      window.scrollTo(0,0);
      $('#AmenitiesDetailsPop').simplePopup();
      });
      $("#btnUpdateAmenitiesDetails").click(function (e) {
      $(".simplePopupClose").click();
      $('#Alertpopup').modal('show');
      });
      $("#btnEditUploadDetails").click(function (e) {
      window.scrollTo(0, 0);
      $('#UploadDetailsPop').simplePopup();
      });
      $("#btnUpdateUploadDetails").click(function (e) {
      $(".simplePopupClose").click();
      $('#Alertpopup').modal('show');
      });
      });