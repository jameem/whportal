$(document).ready(function () {
          
      $('#btnPrevStep').addClass('disabled');
      $("#lnkEdit").addClass('disabled');
      $('#example').DataTable();
      $("#lnkAdd").trigger("click");
      });
      $("#btnok").click(function (e) {
      $('#Alertpopup').modal('hide');
      });
      //Select table row on click
      $("#example tr").click(function (e) {
      var selected = $(this).hasClass("highlight");
      $("#example tr").removeClass("highlight");
      if (!selected)
      $(this).addClass("highlight");
      //Store selected row
      SelectedRow = $(this);
      window.open('/warehousedetails?isedit=true', '_blank');
      });
      $("#example tr").hover(function () {
      $(this).css('cursor', 'pointer');
      }, function () {
      $(this).css('cursor', 'auto');
      });
      $("#lnkEdit").click(function (e) {
      //
      $("#lnkAdd").trigger("click");
      });
      $("#lnkAdd").click(function (e) {
      $("#divStep2").hide();
      $("#divStep3").hide();
      $("#divStep4").hide();
      $("#divStep5").hide();
      $("#divStep6").hide();
      $("#btnNextStep").val('Next');
      $("#divStep1").show();
      $('#AddWarehousePop').simplePopup();
      });
      //Popup
      $("#btnNextStep").click(function (e) {
      if ($('#divStep1').is(':visible'))
      {
      $("#btnNextStep").val('Next');
      $('#btnPrevStep').removeClass('disabled');
      $("#divStep1").hide();
      $("#divStep2").show();
      }
      else if ($('#divStep2').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep2").hide();
      $("#divStep3").show();
      }
      else if ($('#divStep3').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep3").hide();
      $("#divStep4").show();
      }
      else if ($('#divStep4').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep4").hide();
      $("#divStep5").show();
      }
      else if ($('#divStep5').is(':visible')) {
      $("#divStep5").hide();
      $("#divStep6").show();
      $("#btnNextStep").val('Finish');
      }
      else if ($("#btnNextStep").val()==='Finish') {
      $(".simplePopupClose").click();
      $('#Alertpopup').modal('show');
      }
      });
      $("#btnPrevStep").click(function (e) {
      if ($('#divStep2').is(':visible')) {
      $("#btnNextStep").val('Next');
      $('#btnPrevStep').addClass('disabled');
      $("#divStep2").hide();
      $("#divStep1").show();
      }
      else if ($('#divStep3').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep3").hide();
      $("#divStep2").show();
      }
      else if ($('#divStep4').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep4").hide();
      $("#divStep3").show();
      }
      else if ($('#divStep5').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep5").hide();
      $("#divStep4").show();
      }
      else if ($('#divStep6').is(':visible')) {
      $("#btnNextStep").val('Next');
      $("#divStep6").hide();
      $("#divStep5").show();
      }
      });
      jQuery('#btnAddAmmendmentsRow').click(function (event) {
      event.preventDefault();
      var newRow = jQuery('<tr><td><input id="txtExtraAmmendmentName" type="text" class="grid-textbox" /></td><td><input id="txtExtraAmmendmentNarration" type="text" class="grid-textbox" /></td></tr>');
      jQuery('#tblCustoAmmendmends').append(newRow);
      });
      jQuery('#btnAddFilesRow').click(function (event) {
      event.preventDefault();
      var newRow = jQuery('<tr><td><input id="txtCustomFileName" type="text" class="grid-textbox" /></td><td><input id="txtCustomFileExpiryDate" type="date" class="grid-textbox" /></td><td><input id="flCustomeFile" type="file" multiple accept="application/msword,text/plain, application/pdf/*" class="" /></td></tr>');
      jQuery('#tblCustomFiles').append(newRow);
      });