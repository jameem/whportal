var express = require('express');
var router = express.Router();
var mysql=require('mysql');

const db=mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'tutorial'
});

db.connect(function (err){
  if(err){
    throw err;
  }
  console.log('Database connected');
});

router.get('/', function(req, res, next) {
  res.render('myaccount');
  
});

router.post('/add', function(req, res, next) {
  var firstname=req.body.txtFirstName;
  var lastname=req.body.txtLastName;
  var dob=req.body.txtDOB;
  var username=req.body.txtUserName;
  var address=req.body.txtAddress;
  var email=req.body.txtEmail;
  var otherskill=req.body.txtOtherSkills;
  
  // Validation
	req.checkBody('txtFirstName', 'Name is required').notEmpty();
	req.checkBody('txtDOB', 'DOB is required').notEmpty();
	req.checkBody('txtUserName', 'Username is required').notEmpty();
  //req.checkBody('password2', 'Passwords do not match').equals(req.body.password);
  
  //insert
  let post={username:username, password:firstname }
  let sql='INSERT INTO users SET?';
  let query=db.query(sql, post, function(err, result){
    if(err) throw err;
    console.log(result);
    res.render('myaccount');
  });
});
module.exports = router;
