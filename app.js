var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');


var index = require('./routes/index');
var myaccount = require('./routes/myaccount');
var changepassword = require('./routes/changepassword');
var adduser = require('./routes/adduser');
var userpermission = require('./routes/userpermission');
var inventory = require('./routes/inventory');
var managejobs = require('./routes/managejobs');
var enquirydetails = require('./routes/enquirydetails');
var warehouselist = require('./routes/warehouselist');
var warehousedetails = require('./routes/warehousedetails');
var widgets = require('./routes/widgets');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressValidator());

app.use('/', index);
app.use('/myaccount', myaccount);
app.use('/changepassword', changepassword);
app.use('/adduser', adduser);
app.use('/userpermission', userpermission);
app.use('/inventory', inventory);
app.use('/managejobs', managejobs);
app.use('/enquirydetails', enquirydetails);
app.use('/warehouselist', warehouselist);
app.use('/warehousedetails', warehousedetails);
app.use('/widgets', widgets);
app.use('/addpost', myaccount);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
